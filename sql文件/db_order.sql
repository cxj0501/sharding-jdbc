/*
SQLyog Ultimate
MySQL - 5.7.34 : Database - db_order_1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_order_1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `db_order_1`;

/*Table structure for table `config_info` */

CREATE TABLE `config_info` (
  `id` bigint(20) unsigned NOT NULL COMMENT '主键id',
  `config_key` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '配置key',
  `config_value` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '配置value',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `config_info` */

insert  into `config_info`(`id`,`config_key`,`config_value`) values (1644257244021608449,'key1','value1');

/*Table structure for table `order_info_0` */

CREATE TABLE `order_info_0` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `out_trade_no` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单唯一标识',
  `state` varchar(11) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单状态，NEW 未支付订单,PAY已经支付订单,CANCEL超时取消订单',
  `pay_amount` decimal(16,2) DEFAULT NULL COMMENT '订单实际支付金额',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '订单生成时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1644261285237698563 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `order_info_0` */

insert  into `order_info_0`(`id`,`out_trade_no`,`state`,`pay_amount`,`user_id`,`create_time`) values (1644261281479602178,'c439df4e-cca2-4414-adfb-bfffece8','NEW',99.99,1,'2023-04-07 16:50:13');
insert  into `order_info_0`(`id`,`out_trade_no`,`state`,`pay_amount`,`user_id`,`create_time`) values (1644261282070999042,'57bd79e8-cf9d-4ae1-b00c-8e6888c1','NEW',99.99,3,'2023-04-07 16:50:13');
insert  into `order_info_0`(`id`,`out_trade_no`,`state`,`pay_amount`,`user_id`,`create_time`) values (1644261283262181378,'3ec5955d-d81a-491f-9435-1be1e45b','NEW',99.99,9,'2023-04-07 16:50:13');
insert  into `order_info_0`(`id`,`out_trade_no`,`state`,`pay_amount`,`user_id`,`create_time`) values (1644261283715166210,'2f0c047d-d7b8-4f40-83ac-fbfe2056','NEW',99.99,11,'2023-04-07 16:50:14');
insert  into `order_info_0`(`id`,`out_trade_no`,`state`,`pay_amount`,`user_id`,`create_time`) values (1644261284046516226,'cb621816-3b9a-4302-bfc3-29358aba','NEW',99.99,13,'2023-04-07 16:50:14');
insert  into `order_info_0`(`id`,`out_trade_no`,`state`,`pay_amount`,`user_id`,`create_time`) values (1644261284444975106,'e39e9f28-3577-4781-94ea-21c7bf14','NEW',99.99,15,'2023-04-07 16:50:14');
insert  into `order_info_0`(`id`,`out_trade_no`,`state`,`pay_amount`,`user_id`,`create_time`) values (1644261285237698562,'08e5b5ab-1c5d-4aeb-80ad-dcebff81','NEW',99.99,19,'2023-04-07 16:50:14');

/*Table structure for table `order_info_1` */

CREATE TABLE `order_info_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `out_trade_no` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单唯一标识',
  `state` varchar(11) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单状态，NEW 未支付订单,PAY已经支付订单,CANCEL超时取消订单',
  `pay_amount` decimal(16,2) DEFAULT NULL COMMENT '订单实际支付金额',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '订单生成时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1644261284839239682 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `order_info_1` */

insert  into `order_info_1`(`id`,`out_trade_no`,`state`,`pay_amount`,`user_id`,`create_time`) values (1644261282473652225,'bf1a569c-7739-4a71-93b6-aaaa6f3c','NEW',99.99,5,'2023-04-07 16:50:13');
insert  into `order_info_1`(`id`,`out_trade_no`,`state`,`pay_amount`,`user_id`,`create_time`) values (1644261282867916801,'b6c5ce3a-e2e7-44cf-87f1-782010ab','NEW',99.99,7,'2023-04-07 16:50:13');
insert  into `order_info_1`(`id`,`out_trade_no`,`state`,`pay_amount`,`user_id`,`create_time`) values (1644261284839239681,'51699d6c-e421-4b1d-b7fd-6c1f7d33','NEW',99.99,17,'2023-04-07 16:50:14');

/*Table structure for table `order_item_0` */

CREATE TABLE `order_item_0` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单ID',
  `product_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `product_desc` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `order_item_0` */

/*Table structure for table `order_item_1` */

CREATE TABLE `order_item_1` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单ID',
  `product_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `product_desc` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `order_item_1` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
