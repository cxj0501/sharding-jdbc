package com.wystudy;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wystudy.mapper.ConfigInfoMapper;
import com.wystudy.mapper.OrderInfoMapper;
import com.wystudy.model.ConfigInfo;
import com.wystudy.model.OrderInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@SpringBootTest(classes = DemoApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class ShardingTest {

    @Autowired
    private OrderInfoMapper orderInfoMapper;
    @Autowired
    private ConfigInfoMapper configInfoMapper;

    @Test
    public void testSharding(){

        for(int i=0; i<20; i++){
            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setOutTradeNo(UUID.randomUUID().toString().substring(0,32));
            orderInfo.setCreateTime(new Date());
            orderInfo.setPayAmount(99.9);
            orderInfo.setState("NEW");
            orderInfo.setUserId(Long.valueOf(i));
            orderInfoMapper.insert(orderInfo);
        }
    }


    @Test
    public void testBroadcast(){

        ConfigInfo configInfo = new ConfigInfo();
        configInfo.setConfigKey("key1");
        configInfo.setConfigValue("value1");
        configInfoMapper.insert(configInfo);
    }


    @Test
    public void testShardingDb(){

        for(int i=0; i<20; i++){
            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setOutTradeNo(UUID.randomUUID().toString().substring(0,32));
            orderInfo.setCreateTime(new Date());
            orderInfo.setPayAmount(99.9);
            orderInfo.setState("NEW");
            orderInfo.setUserId(Long.valueOf(new Random().nextInt(20) + ""));
            orderInfoMapper.insert(orderInfo);
        }
    }


    @Test
    public void testBind(){

        orderInfoMapper.getOrderItemById();
    }


    @Test
    public void testStandardPrecise(){

        OrderInfo orderInfo = orderInfoMapper.selectById(862811408507600897L);
        System.out.println(orderInfo);
    }


    @Test
    public void testStandardPreciseDb(){
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOutTradeNo(UUID.randomUUID().toString().substring(0,32));
        orderInfo.setCreateTime(new Date());
        orderInfo.setPayAmount(99.9);
        orderInfo.setState("NEW");
        orderInfo.setUserId(2l);
        orderInfoMapper.insert(orderInfo);

    }


    @Test
    public void testStandardRange(){
        List<OrderInfo> list = orderInfoMapper.selectList(new QueryWrapper<OrderInfo>().between("id", 1L, 1L));

    }


    @Test
    public void testHint(){
        HintManager.clear();
        HintManager hintManager = HintManager.getInstance();
        //假设指定sql到ds1中的order_info_1表中去执行
        hintManager.addDatabaseShardingValue("order_info", 1L);
        hintManager.addTableShardingValue("order_info", 1L);
        hintManager.addTableShardingValue("order_info", 2L);
        orderInfoMapper.selectById(2L);
    }

    @Test
    public void testComplex(){
        orderInfoMapper.selectList(new QueryWrapper<OrderInfo>().in("id",1L, 8L).eq("user_id",2L));
    }

}
