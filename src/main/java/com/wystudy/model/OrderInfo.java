package com.wystudy.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class OrderInfo {

    @TableId(value = "id", type = IdType.AUTO)
    //@TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    private String outTradeNo;
    private String state;
    private Date createTime;
    private Double payAmount;
    private Long userId;

}
