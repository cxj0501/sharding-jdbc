package com.wystudy.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class ConfigInfo {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    private String configKey;
    private String configValue;
}
