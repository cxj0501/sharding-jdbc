package com.wystudy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wystudy.model.OrderInfo;
import org.apache.ibatis.annotations.Select;

public interface OrderInfoMapper extends BaseMapper<OrderInfo>{


    @Select("select t1.* from order_info t1 join order_item t2 on t1.id = t2.order_id where t1.id = 1")
    OrderInfo getOrderItemById();
}
