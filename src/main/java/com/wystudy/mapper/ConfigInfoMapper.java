package com.wystudy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wystudy.model.ConfigInfo;
import com.wystudy.model.OrderInfo;

public interface ConfigInfoMapper extends BaseMapper<ConfigInfo>{
}
