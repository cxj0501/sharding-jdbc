package com.wystudy.strategy;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;

public class CustomDbPreciseShardingAlgorithm implements PreciseShardingAlgorithm<Long>{


    /**
     * order_info，路由规则：id % 真实表的数量
     * @param dataSourseSet  数据源的合集，分库时表示所有库的合集，如果是分表，表示所有真实表的合集
     * @param preciseShardingValue 分片属性的对象，包括三个属性：
     *                             logicTableName：逻辑表
     *                             columnName：分片键
     * @return
     */
    @Override
    public String doSharding(Collection<String> dataSourseSet, PreciseShardingValue<Long> preciseShardingValue) {
        Long user_id = preciseShardingValue.getValue();
        int size = dataSourseSet.size();
        String value = user_id % size + ""; //不是0就是1
        for(String dataSource : dataSourseSet){
            if(dataSource.endsWith(value)){
                return dataSource;
            }
        }
        return null;
    }
}
