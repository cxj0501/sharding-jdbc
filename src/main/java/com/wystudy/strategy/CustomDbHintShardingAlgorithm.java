package com.wystudy.strategy;

import org.apache.shardingsphere.api.sharding.hint.HintShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.hint.HintShardingValue;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

public class CustomDbHintShardingAlgorithm implements HintShardingAlgorithm<Long>{



    /**
     * order_info，路由规则：id % 真实表的数量
     * @param dataSourseSet  数据源的合集，分库时表示所有库的合集，如果是分表，表示所有真实表的合集
     * @param hintShardingValue 分片属性的对象，包括三个属性：
     *                             logicTableName：逻辑表
     *                             columnName：分片键
     *                             values: 非分片键的值，可以是任意值
     * @return
     */
    @Override
    public Collection<String> doSharding(Collection<String> dataSourseSet, HintShardingValue<Long> hintShardingValue) {
        Set<String> result = new LinkedHashSet<>();
        for(long shardingValue : hintShardingValue.getValues()){
            String value = shardingValue % dataSourseSet.size() + "";
            for(String dataSource : dataSourseSet){
                if(dataSource.endsWith(value)){
                    result.add(dataSource);
                }
            }
        }
        return result;
    }
}
