package com.wystudy.strategy;

import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

public class CustomRangeShardingAlgorithm implements RangeShardingAlgorithm<Long>{


    /**
     * order_info，路由规则：id % 真实表的数量
     * @param dataSourseSet  数据源的合集，分库时表示所有库的合集，如果是分表，表示所有真实表的合集
     * @param rangeShardingValue 分片属性的对象，包括三个属性：
     *                             logicTableName：逻辑表
     *                             columnName：分片键
     * @return
     */
    @Override
    public Collection<String> doSharding(Collection<String> dataSourseSet, RangeShardingValue<Long> rangeShardingValue) {
        Set<String> result = new LinkedHashSet<>();
        Long minValue = rangeShardingValue.getValueRange().lowerEndpoint();
        Long maxValue = rangeShardingValue.getValueRange().upperEndpoint();
        for(long i=minValue; i<=maxValue; i++){
            String value = i % dataSourseSet.size() + "";
            for(String dataSource : dataSourseSet){
                if(dataSource.endsWith(value)){
                    result.add(dataSource);
                }
            }
        }
        return result;
    }
}
